This repository gives access to the exact architectures used in the paper "Learning to rank music tracks using triplet loss (L. Prétet, G. Richard, G. Peeters)". The `models.py` file contains the three architectures (AT, TL ans TL Autopool) to reproduce our experiments, as well as the triplet loss, custom layer and a small test on artificial data.  

The training and evaluation code can be released as well, for research purpose only. To request access to the extended version, please write an email to: laure@bridge.audio
