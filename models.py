# -*- coding: utf-8 -*-

# @Author: laure
# @Date:   2019-10-15 11:01:27

"""
Auto-tagger (AT) and Triplet loss (TL) architectures for music similarity training.
If called directly, this script instatiates and trains each architecture on artificial data
(for test purpose).
"""

import sys

import numpy as np
import tensorflow as tf

from keras import backend as K
from keras.layers import Input, Dense, Layer
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import concatenate, TimeDistributed
from keras.layers import Flatten, Dropout
from keras.models import Model
from keras.engine.topology import InputSpec
from keras import initializers
from keras import constraints
from keras import regularizers
from keras.models import load_model

################## TRIPLET LOSS #############################

def triplet_loss(alpha = 0.5, output_dim=128):
    """
    Implementation of the triplet loss function.
    Arguments:
    y_true -- true labels, required when you define a loss in Keras, you don't need it in this function.
    y_pred -- python list containing three objects:
            anchor -- the encodings for the anchor data
            positive -- the encodings for the positive data (similar to anchor)
            negative -- the encodings for the negative data (different from anchor)
    Returns:
    loss -- real number, value of the loss
    """
    
    def loss(y_true, y_pred):

        positive = y_pred[:,0:output_dim]
        anchor = y_pred[:,output_dim:2*output_dim]
        negative = y_pred[:,2*output_dim:3*output_dim]

        # distance between the anchor and the positive
        pos_dist = K.sum(K.square(anchor-positive),axis=1)

        # distance between the anchor and the negative
        neg_dist = K.sum(K.square(anchor-negative),axis=1)

        # compute loss
        basic_loss = pos_dist-neg_dist+alpha
        basic_loss = K.maximum(basic_loss,0.0)
     
        return basic_loss

    return loss

################## SPECIAL LAYERS #############################

class UnitNormLayer(Layer):

    def build(self, input_shape):
        super(UnitNormLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x = tf.nn.l2_normalize(x, axis=1)
        return x

class AutoPool1D(Layer):
    '''
    Automatically tuned soft-max pooling.
    This layer automatically adapts the pooling behavior to interpolate
    between mean- and max-pooling for each dimension.

    This layer was taken and adapted from Brian McFee's implementation: 
    https://github.com/marl/autopool/blob/master/autopool/autopool.py
    '''
    def __init__(self, axis=0,
                 kernel_initializer='zeros',
                 kernel_constraint=None,
                 kernel_regularizer=None,
                 **kwargs):
        '''
        Parameters
        ----------
        axis : int
            Axis along which to perform the pooling. By default 0 (should be time).
        kernel_initializer: Initializer for the weights matrix
        kernel_regularizer: Regularizer function applied to the weights matrix
        kernel_constraint: Constraint function applied to the weights matrix
        kwargs
        '''

        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'), )

        super(AutoPool1D, self).__init__(**kwargs)

        self.axis = axis
        self.kernel_initializer = initializers.get(kernel_initializer)
        self.kernel_constraint = constraints.get(kernel_constraint)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.input_spec = InputSpec(min_ndim=3)
        self.supports_masking = True

    def build(self, input_shape):
        assert len(input_shape) >= 3
        input_dim = input_shape[-1]

        self.kernel = self.add_weight(shape=(1, input_dim),
                                      initializer=self.kernel_initializer,
                                      name='kernel',
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
        self.input_spec = InputSpec(min_ndim=2, axes={-1: input_dim})
        self.built = True

    def compute_output_shape(self, input_shape):
        return self.get_output_shape_for(input_shape)

    def get_output_shape_for(self, input_shape):
        shape = list(input_shape)
        del shape[self.axis]
        return tuple(shape)

    def get_config(self):
        config = {'kernel_initializer': initializers.serialize(self.kernel_initializer),
                  'kernel_constraint': constraints.serialize(self.kernel_constraint),
                  'kernel_regularizer': regularizers.serialize(self.kernel_regularizer),
                  'axis': self.axis}

        base_config = super(AutoPool1D, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def call(self, x, mask=None):
        scaled = self.kernel * x
        max_val = K.max(scaled, axis=self.axis, keepdims=True)
        softmax = K.exp(scaled - max_val)
        weights = softmax / K.sum(softmax, axis=self.axis, keepdims=True)
        return K.sum(x * weights, axis=self.axis, keepdims=False)

################# 3 NETWORK ARCHITECTURES ##############################

def create_base_network(input_shape, output_dim=128, variant='tl'):
    """
    Instantiates Choi's Auto_tagger architecture, adapted and simplified from: https://arxiv.org/abs/1606.00298

    The architecture is inspired from FCN5, as described in: 
    Keunwoo Choi, George Fazekas, and Mark Sandler. Automatic tagging
    using deep convolutional neural networks. 2016 (https://arxiv.org/abs/1606.00298)
    The code of which could be found at:
    https://github.com/keunwoochoi/music-auto_tagging-keras

    The main modifications compared to Choi's implementation are:
    - adapted pooling sizes to a input shape of 96x512x1
    - no Batch Normamization
    - Dropout instead
    - ELU instead of ReLu in the first layer

    Args:
        input_shape (tuple of 3 int): First and second dimensions of the feature image, plus number of channels.
        output_dim (int): Dimension of the output vector.
        variant (str): 'at' or 'tl', depending if the network is trained for auto-tagging or similarity learning.

    Returns:
      model (Keras model instance): The model, not compiled.
    """

    # Input block
    input_layer = Input(shape=input_shape) # 96 x 512 x 1
    
    # Conv block 1
    x = Conv2D(128, (3, 3), padding='same', name='conv1', activation='elu')(input_layer)
    x = MaxPooling2D(pool_size=(2, 4), name='pool1')(x)
    # Conv block 2
    x = Conv2D(256, (3, 3), padding='same', name='conv2', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 4), name='pool2')(x)
    x = Dropout(0.1)(x)
    # Conv block 3
    x = Conv2D(512, (3, 3), padding='same', name='conv3', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 4), name='pool3')(x)
    x = Dropout(0.2)(x)
    # Conv block 4
    x = Conv2D(1024, (3, 3), padding='same', name='conv4', activation='relu')(x)
    x = MaxPooling2D(pool_size=(3, 3), name='pool4')(x)
    x = Dropout(0.25)(x)
    # Conv block 5
    x = Conv2D(2048, (3, 3), padding='same', name='conv5', activation='relu')(x)
    x = MaxPooling2D(pool_size=(4, 2), name='pool5')(x)
    x = Dropout(0.3)(x)

    # Output
    x = Flatten(name='embedding')(x)
    if variant == 'tl':
        # Triplet Loss network has a linear activation and the embedding are
        # constrained to the unit circle.
        x = Dense(output_dim, name='output')(x)
        x = UnitNormLayer()(x)
    else:
        x = Dense(output_dim, activation='sigmoid', name='output')(x)

    # Create model
    model = Model(input_layer, x)

    return model

def create_network_TL_Autopool(input_shape, output_dim=128):
    """
    This variant uses the Autopooling layer presented by Brian McFee in:
    http://www.justinsalamon.com/uploads/4/3/9/4/4394963/mcfee_autopool_taslp_2018.pdf

    Compared to TL, the pooling shapes are adapted to keep a temporal information until the 
    last layer. It also has twice less filters because of GPU memory constraints.

    Args:
        input_shape (tuple of 3 int): First and second dimensions of the feature image, plus number of channels.
        output_dim (int): Dimension of the output vector.
    Returns:
      model (Keras model instance): The model, not compiled.
    """

    # Input block
    print("Input shape:", input_shape)
    input_layer = Input(shape=input_shape) # 96 x 512 x 1
    
    # Conv block 1
    x = Conv2D(64, (3, 3), padding='same', name='conv1', activation='elu')(input_layer)
    x = MaxPooling2D(pool_size=(2, 2), name='pool1')(x)
    # Conv block 2
    x = Conv2D(128, (3, 3), padding='same', name='conv2', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool2')(x)
    x = Dropout(0.1)(x)
    # Conv block 3
    x = Conv2D(256, (3, 3), padding='same', name='conv3', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool3')(x)
    x = Dropout(0.2)(x)
    # Conv block 4
    x = Conv2D(512, (3, 3), padding='same', name='conv4', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool4')(x)
    x = Dropout(0.25)(x)
    # Conv block 5
    x = Conv2D(1024, (3, 3), padding='same', name='conv5', activation='relu')(x)
    x = MaxPooling2D(pool_size=(6, 1), name='pool5')(x)
    x = Dropout(0.3)(x)

    # Output
    x = TimeDistributed(Dense(output_dim), name='dense')(x)
    x = AutoPool1D(axis=2, name='autopool')(x)
    x = Flatten(name='embedding')(x)

    # Create model
    model = Model(input_layer, x)

    return model

################## GENERIC BUILD METHODS ################################

def build(input_shape, variant='tl', output_dim=128):
    """
    This method instantiates the network architectures for training.
    In the Auto-tagging case (AT), this method simply returns the model from the create_base_network
    function.
    In the Triplet Loss case (TL), the method creates a 3-branch network.

    Args:
        input_shape (tuple of 3 int): First and second dimensions of the feature image, plus number of channels.
        variant (str): Tells which model must be built.
        input_shape (tuple of 3 int): First and second dimensions of the feature image, plus number of channels.

    Returns:
      model (Keras model instance): The model, not compiled.
    """

    if variant == 'at':
        Shared_DNN = create_base_network_at(input_shape)
        return model

    anchor_input = Input(input_shape, name='anchor_input')
    positive_input = Input(input_shape, name='positive_input')
    negative_input = Input(input_shape, name='negative_input')

    if variant == 'tl':
        Shared_DNN = create_base_network(input_shape, output_dim, variant='tl')
    elif variant == 'tl_ap':
        Shared_DNN = create_network_TL_Autopool(input_shape, output_dim)

    encoded_anchor = Shared_DNN(anchor_input)
    encoded_positive = Shared_DNN(positive_input)
    encoded_negative = Shared_DNN(negative_input)
    merged_vector = concatenate([encoded_positive, encoded_anchor, encoded_negative], axis=-1, name='merged_layer')
    model = Model(inputs=[positive_input, anchor_input, negative_input], outputs=merged_vector)

    return model

def get_inference_model(input_shape, model_file, variant='tl', output_dim=128):
    """
    This method instantiates the network architectures for inference.
    In the Auto-tagging case (AT), this method simply loads the model from the model_file.
    In the Triplet Loss case (TL), the method creates a 1-branch network with the trained weights.

    Args:
        input_shape (tuple of 3 int): First and second dimensions of the feature image, plus number of channels.
        variant (str): Tells which model must be built.
        input_shape (tuple of 3 int): First and second dimensions of the feature image, plus number of channels.

    Returns:
      model (Keras model instance): The model, not compiled.
    """

    if variant == 'at':
        model = load_model(model_file)
        return model

    anchor_input = Input(input_shape, name='anchor_input')
    
    if variant == 'tl':
        Shared_DNN = create_base_network(input_shape, output_dim, variant='tl')
    elif variant == 'tl_ap':
        Shared_DNN = create_network_TL_Autopool(input_shape, output_dim)

    encoded_anchor = Shared_DNN(anchor_input)
    model = Model(inputs=anchor_input, outputs=encoded_anchor)

    model.load_weights(model_file)
    return model

########################### TESTS ###################################

if __name__ == '__main__':

    # If called directly, this script instantiates the AT and TL architectures
    # and tests them on randomly generated data.

    # Build AT model and test with artificial data
    model = build((96,512,1), variant='at', output_dim=488)
    print("##### Auto-tagger architecture: #######")
    print(model.summary())
    model.compile(loss='binary_crossentropy', optimizer="adam")
    x_ram = np.random.rand(100, 96, 512, 1)
    y_ram = np.random.rand(100, 488)
    x_ram_val = np.random.rand(100, 96, 512, 1)
    y_ram_val = np.random.rand(100, 488)
    history = model.fit(
                x_ram,
                y_ram,
                batch_size=1,
                epochs=2,
                verbose=1,
                validation_data=(x_ram_val, y_ram_val)
                )
    print("##### Auto-tagger trained on artificial data: Test success ! #######")

    # Build TL model and test with artificial data
    model = build((96,512,1), variant='tl', output_dim=128)
    print("##### TL system architecture: #######")
    print(model.summary())
    model.compile(loss=triplet_loss(output_dim=128), optimizer="adam")
    x_ram = np.random.rand(3, 100, 96, 512, 1)
    x_ram = [x_ram[0,:,:,:,:], x_ram[1,:,:,:,:], x_ram[2,:,:,:,:]]
    y_ram = np.random.rand(100, 128)
    x_ram_val = np.random.rand(3, 100, 96, 512, 1)
    x_ram_val = [x_ram_val[0,:,:,:,:], x_ram_val[1,:,:,:,:], x_ram_val[2,:,:,:,:]]
    y_ram_val = np.random.rand(100, 128)
    history = model.fit(
                x_ram,
                y_ram,
                batch_size=1,
                epochs=2,
                verbose=1,
                validation_data=(x_ram_val, y_ram_val)
                )
    print("##### TL system trained on artificial data: Test success ! #######")

    # Build TL Autopool model and test with artificial data
    model = build((96,512,1), variant='tl_ap', output_dim=128)
    print("##### TL Autopool system architecture: #######")
    print(model.summary())
    model.compile(loss=triplet_loss(output_dim=128), optimizer="adam")
    history = model.fit(
                x_ram,
                y_ram,
                batch_size=1,
                epochs=2,
                verbose=1,
                validation_data=(x_ram_val, y_ram_val)
                )
    print("##### TL Autopool system trained on artificial data: Test success ! #######")







